# Dies ist eine Test-OER

foo bar

## Lizenz

![Creative Commons License](https://i.creativecommons.org/l/by/4.0/88x31.png) This work is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

<script type="application/ld+json">
{
   "@context":"http://schema.org",
   "type":"CreativWork",
   "keywords": [
      "OER",
      "Mathe"
   ],
   "url":"https://example.org/oer/",
   "author":{
      "@type":"Person",
      "name":"Tobias Steineroer",
      "sameAs": [
         "https://oerworldmap.org/resource/urn:uuid:0e3d180e-73c3-42ec-8971-aabe20d4fc50",
         "https://orcid.org/0000-0002-3158-3136",
         "https://twitter.com/cmplxtv_studies"
      ]
   }
}
</script>